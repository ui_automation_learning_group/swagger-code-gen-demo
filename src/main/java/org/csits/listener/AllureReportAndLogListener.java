package org.csits.listener;

import org.testng.ITestContext;
import org.testng.TestListenerAdapter;

import io.qameta.allure.okhttp.AllureOkHttp;
import io.swagger.client.Configuration;

/**
 * 通过 实现TestListenerAdapter适配器，重新 onStart方法开启debug模式和allure报告数据收集
 * @author douliyou
 *
 */
public class AllureReportAndLogListener extends TestListenerAdapter{
	
	@Override
	public void onStart(ITestContext testContext) {
		super.onStart(testContext);
		Configuration.getDefaultApiClient().setDebugging(true);
		Configuration.getDefaultApiClient().getHttpClient().interceptors().add(new AllureOkHttp());
	}
	
}
